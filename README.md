importdir.py
==========

A simple python module for importing a whole directory of modules in only two lines:

```python
import importdir
importdir.do("path/to/my/directory", globals())
```

## Idea

Imagine you have the following structure:

```asciidoc
example_dir/
    hello.py
    world.py
```

**You want** to be able to include all python modules from *example_dir*, which are developed and maintained by your buddies.

In contrary to common solutions described [here](http://stackoverflow.com/questions/1057431/loading-all-modules-in-a-folder-in-python), **you don't want**:

* to bother your buddies with letting them maintain an `__init__.py` file with the exhaustive list of their modules;
* to bother your buddies with any file in their directory and telling them "please, please, don't touch that file";
* that import to cost you more than two lines.

## Hello world

### Their job

`example_dir/hello.py:`

    def speak():
        print "hello"

`example_dir/world.py:`

    def name():
        print "world"

### Your job

1. Download `importdir.py`.
2. Type

`example_main.py:`

```python
import importdir
importdir.do("example_dir", globals())

hello.speak()
world.name()
```

Which gives you:

```asciidoc
hello
world
```

Here you go :)

## Initial author

[Aurelien Lourot](http://lourot.com/)
